#!/bin/bash

APPD_PLATFORM_SETUP="/tmp/platform-setup.sh"
APPD_PLATFORM_ADMIN="$APPD_PLATFORM/platform-admin"
APPD_PLATFORM_ADMIN_CLI="$APPD_PLATFORM_ADMIN/bin/platform-admin.sh"

apt-get update
apt-get install --fix-missing -q -y libaio1 libnuma1 net-tools curl tar tzdata
echo "ulimit -n 65535" >> /etc/profile
echo "ulimit -u 8192" >> /etc/profile
echo "vm.swappiness = 10" >> /etc/sysctl.conf
curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=$USER&password=$PASSWORD" https://login.appdynamics.com/sso/login/
curl -L -o $APPD_PLATFORM_SETUP -b /tmp/cookies.txt $BASEURL/enterprise-console/$VERSION/platform-setup-x64-linux-$VERSION.sh
chmod +x $APPD_PLATFORM_SETUP
sed -i "s/\(serverHostName=\)\$/\1$HOSTNAME/" /tmp/pa.response
$APPD_PLATFORM_SETUP -q -varfile /tmp/pa.response -dir $APPD_PLATFORM
$APPD_PLATFORM_ADMIN_CLI create-platform --name appd --installation-dir $APPD_PLATFORM
$APPD_PLATFORM_ADMIN_CLI add-hosts --hosts localhost
$APPD_PLATFORM_ADMIN_CLI submit-job --service controller --job install --arg-file /tmp/controller.response

exit 0