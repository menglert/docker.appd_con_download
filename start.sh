#!/bin/bash

rm -rf $APPD_PLATFORM/controller/appserver/glassfish/domains/domain1/osgi-cache

find $APPD_PLATFORM/ -name *.log -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.lock -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.lck -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.pid -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.id -exec /bin/bash -c "rm -rf {}" \;

find $APPD_PLATFORM/controller/db/ -name *.MYI -exec /bin/bash -c "echo {} | xargs $APPD_PLATFORM/controller/db/bin/myisamchk" \;
find $APPD_PLATFORM/controller/db/ -name *.MYI -exec /bin/bash -c "echo {} | sed \"s/.MYI//\" | xargs $APPD_PLATFORM/controller/db/bin/myisamchk" \;

$APPD_PLATFORM/controller/bin/controller.sh start

tail -f /dev/null