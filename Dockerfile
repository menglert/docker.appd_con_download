FROM ubuntu AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG BASEURL="https://download.appdynamics.com/download/prox/download-file"
ARG VERSION
ARG USER
ARG PASSWORD

ENV APPD_PLATFORM="/opt/appdynamics/platform"

ADD pa.response /tmp/
ADD controller.response /tmp/
ADD install_con.sh /tmp/

RUN chmod +x /tmp/*.sh
RUN /tmp/install_con.sh


FROM ubuntu
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV APPD_PLATFORM="/opt/appdynamics/platform"

COPY --from=builder ${APPD_PLATFORM}/controller ${APPD_PLATFORM}/controller
COPY --from=builder ${APPD_PLATFORM}/jre ${APPD_PLATFORM}/jre

ADD start.sh ${APPD_PLATFORM}/controller/

RUN apt-get update \
    && apt-get install --fix-missing -q -y libaio1 libnuma1 lsof \
    && update-alternatives --install "/usr/bin/java" "java" "$(find ${APPD_PLATFORM}/jre/ -name java)" 1 \
    && echo "ulimit -n 65535" >> /etc/profile \
    && echo "ulimit -u 8192" >> /etc/profile \
    && echo "vm.swappiness = 10" >> /etc/sysctl.conf

EXPOSE 8090 8181

CMD [ "/bin/bash", "-c", "${APPD_PLATFORM}/controller/start.sh" ]